import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import Appbar from './components/Appbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';

import Products from './pages/Products';

function App() {
  return (
    <>
      <Router>

        {/* <Appbar /> */}

        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/products' component={Products} />
        </Switch>
      </Router>
    </>
  );
}

export default App;