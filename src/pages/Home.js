import React from 'react';
import CalendarProg from '../components/CalendarProg';

import 'react-big-calendar/lib/css/react-big-calendar.css';
// import "react-big-calendar/lib/addons/dragAndDrop/styles.css";


function Home() {
    return (
        <div className='calendar'>

            <CalendarProg />
        </div>
    );
}

export default Home;