import React, { useState } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import moment from 'moment';
require('moment/locale/es.js');


const localizer = momentLocalizer(moment)
const DnDCalendar = withDragAndDrop(Calendar);


const myEventsList = [{
    title: "today",
    start: new Date('2020-09-05 10:22:00'),
    end: new Date('2020-09-05 10:42:00')
},
{
    title: "string",
    start: new Date('2020-09-15 12:22:00'),
    end: new Date('2020-09-15 13:42:00')
}]
// const onEventResize = (data) => {
//     // const { start, end } = data;

//     // this.setState((state) => {
//     //     state.events[0].start = start;
//     //     state.events[0].end = end;
//     //     return { events: state.events };
//     // });
// };

const onEventDrop = (det) => {
    console.log(det);
};
const onEventResize = (data) => {

    // const { start, end } = data;

    // this.setState((state) => {
    //     state.events[0].start = start;
    //     state.events[0].end = end;
    //     return { events: state.events };
    // });
};
const CalendarProg = () => {
    return (
        <div style={{ height: `${600}px` }} className="bigCalendar-container">
            <DnDCalendar
                //defaultDate={moment().toDate()}
                //defaultView="month"
                localizer={localizer}
                events={myEventsList}
                startAccessor="start"
                endAccessor="end"
                onEventDrop={(det) => onEventDrop(det)}
                onEventResize={() => onEventResize()}
                resizable
                style={{ height: "60vh" }}
            />
        </div>
    );
}

export default CalendarProg;
